﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CodeCounterX
{
    class CodeCounter
    {
        public string Key { get; set; }
        public int Value { get; set; }

        public CodeCounter(string key, int value)
        {
            this.Key = key;
            this.Value = value;
        }
    }
}
