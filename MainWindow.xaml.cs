﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace CodeCounterX
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void reportBack(object sender, RoutedEventArgs e)
        {
            string path = PathString.Text;
            if(path.Equals(null) || path == "")
            {
                System.Windows.MessageBox.Show("路径选择不能为空", "警告", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            int selectType = this.TypeSelector.SelectedIndex;
            List<string> suffixs = new List<string>();
            switch (selectType)
            {
                case 0:
                    suffixs.Add(".js");
                    suffixs.Add(".vue");
                    suffixs.Add(".cpp");
                    suffixs.Add(".java");
                    suffixs.Add(".py");
                    suffixs.Add(".cs");
                    break;
                case 1:
                    suffixs.Add(".cpp");
                    break;
                case 2:
                    suffixs.Add(".java");
                    break;
                case 3:
                    suffixs.Add(".js");
                    break;
                case 4:
                    suffixs.Add(".vue");
                    break;
                case 5:
                    suffixs.Add(".py");
                    break;
                case 6:
                    suffixs.Add(".cs");
                    break;
                default:
                    break;
            }

            List<FileInfo> codes = new List<FileInfo>();
            GetCodeFiles(codes, path, suffixs);
            int codeFileNums = codes.Count;

            int[] lineInfo = new int[2];
            foreach(FileInfo codeFile in codes)
            {
                StreamReader reader = new StreamReader(codeFile.FullName);
                string temp = reader.ReadLine();
                while (temp != null)
                {
                    temp = temp.Trim();
                    lineInfo[0]++;
                    try
                    {
                        if(temp != "")
                        {
                            lineInfo[1]++;
                        }
                    } catch(Exception)
                    {

                    }
                    temp = reader.ReadLine();
                }
            }

            Report rWindow = new Report();
            rWindow.Ls.ItemsSource = new CodeCounter[]
            {
                new CodeCounter("选定类型代码文件数", codeFileNums),
                new CodeCounter("完全代码行数", lineInfo[0]),
                new CodeCounter("不含空行的代码行数", lineInfo[1])
            };
            rWindow.Show();
            rWindow.Topmost = true;
            rWindow.Owner = this;   //Set MainWindow
        }

        private void pickFile(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.Description = "Select a path";
            if(folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.PathString.Text = folderBrowserDialog.SelectedPath;
            }
        }

        /**
         * 用于获取所有符合检索条件的代码文件
         * @Author 梁源
         */
        private void GetCodeFiles(List<FileInfo> codes, string path, List<string> suffixs)
        {
            DirectoryInfo root = new DirectoryInfo(path);

            FileInfo[] list = root.GetFiles();
            if(list != null)
            {
                foreach(FileInfo file in list)
                {
                    foreach(string suffix in suffixs)
                    {
                        if(file.Name.EndsWith(suffix))
                        {
                            codes.Add(file);
                        }
                    }
                }
            }
            DirectoryInfo[] childRoots = root.GetDirectories();
            if(childRoots != null)
            {
                foreach(DirectoryInfo directory in childRoots)
                {
                    GetCodeFiles(codes, directory.FullName, suffixs);
                }
            }
        }
    }
}
